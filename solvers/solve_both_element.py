import cv2
import numpy as np
import math
from functions import *
from matplotlib import pyplot as plt
from pytesseract import image_to_string


from sklearn.preprocessing import StandardScaler

mylty_size = 3

image = cv2.imread("samples/3.png")
image = dyn_resize(image.copy(), mylty_size * 100)

image_grey = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
bl = cv2.medianBlur(image_grey, 5)

ret, thresh_img = cv2.threshold(image, 181, 255, cv2.THRESH_BINARY)
bg = cv2.Canny(thresh_img, 10, 250)

kernel = np.ones((10, 10),np.uint8)
closing = cv2.morphologyEx(bg, cv2.MORPH_CLOSE, kernel)



bg = cv2.Canny(closing, 1, 1)
blurred = cv2.blur(bg,(2,2))
ret, thresh_img = cv2.threshold(blurred, 19, 255, cv2.THRESH_BINARY)

contours, hierarchy = cv2.findContours(thresh_img, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
print(len(contours))

x = [contour for contour in contours if len(contour) > mylty_size * 45 ]
print(len(x))

img_contours = np.uint8(np.zeros(image.shape))
cv2.drawContours(img_contours, x, -1, (255,255,255), 2)




for i in x:
	element = get_element(img_contours, i)
	image_grey = cv2.cvtColor(element, cv2.COLOR_BGR2GRAY)
	scaler = StandardScaler()
	caled_x = scaler.fit_transform(image_grey)
	result = caled_x.mean() * (10 ** 18)

	border = get_border(i)
	print(result)
	cv2.putText(img_contours, str(result)[:4], (border[0][0],border[0][1]), cv2.FONT_HERSHEY_SIMPLEX, 0.3 * mylty_size, (0,0,255), 2)

cv2.imwrite('new.png', img_contours)

# cv2.imwrite('new.png', blurred)

