import cv2
import numpy as np
from functions import url_to_img

def solve_slider_captcha(template = None, bg = None, offset = 10):

  template = url_to_img("https://necaptcha.nosdn.127.net/977082363c56416dadf63830b4fc267c.png")
  bg = url_to_img("https://necaptcha.nosdn.127.net/ffc03351ead24f3bbef5c2d1280bf2fe.jpg")
  image_grey = cv2.cvtColor(template, cv2.COLOR_BGR2GRAY)
  _, thresh_img = cv2.threshold(image_grey, 10, 255, cv2.THRESH_BINARY)
  template = cv2.Canny(thresh_img,0,0)

  image_grey = cv2.cvtColor(bg, cv2.COLOR_BGR2GRAY)
  bg = cv2.Canny(bg,10,190)

  result = cv2.matchTemplate(bg, template, cv2.TM_CCOEFF_NORMED)
  (minVal, maxVal, minLoc, maxLoc) = cv2.minMaxLoc(result)

  return maxLoc[0] + offset