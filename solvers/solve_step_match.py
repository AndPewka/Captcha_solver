import cv2
import numpy as np
import math
from functions import *
from matplotlib import pyplot as plt
from pytesseract import image_to_string
from sklearn.preprocessing import StandardScaler


image = cv2.imread("samples/match/5.png")

image = dyn_resize(image, 100)
template = image[345:384,0:150]

bg = image[0:345, 0:image.shape[0]]
bg = cv2.Canny(bg,611,251)
# bg = cv2.Canny(bg,17,91)

image_grey = cv2.cvtColor(template, cv2.COLOR_BGR2GRAY)
_, thresh_img = cv2.threshold(image_grey, 201, 255, cv2.THRESH_BINARY)
template = cv2.Canny(thresh_img,0,0)

times = template.shape[1] // 36
templates = []


for i in range(times):
  templates.append(template[0:template.shape[1],i*36:(1 + i)*36])

result = cv2.matchTemplate(bg, dyn_resize(templates[2], 185), cv2.TM_CCOEFF_NORMED)
(minVal, maxVal, minLoc, maxLoc) = cv2.minMaxLoc(result)

rectangle(image, dyn_resize(templates[2], 185), minLoc, maxLoc)


# show_image(image)

cv2.imwrite('new.png', image)
