import cv2
import numpy as np
import math
from functions import *
from matplotlib import pyplot as plt
from pytesseract import image_to_string


image = url_to_img("https://uniportal.huawei.com/accounts1/rest/hwidcenter/uid-code")

# image = cv2.imread("samples/13.png")
image = dyn_resize(image.copy(), 400)

def my_func(image):

  image_grey = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
  median_image  = cv2.medianBlur(image_grey, 7)
  ret, thresh_img = cv2.threshold(median_image, 100, 255, cv2.THRESH_BINARY)
  return thresh_img

def func(image):
  gry = cv2.cvtColor(image.copy(), cv2.COLOR_BGR2GRAY)
  cls = cv2.morphologyEx(gry, cv2.MORPH_CLOSE, None)
  ret, thresh_img = cv2.threshold(cls, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
  # median_image  = cv2.medianBlur(thresh_img, 13)
  return thresh_img



line = np.concatenate((cv2.cvtColor(image, cv2.COLOR_BGR2GRAY),func(image), my_func(image)), axis=0)

cv2.imwrite('new.png', line)
