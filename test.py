import numpy as np
from functions import *
import cv2
from time import sleep
def nothing(x):
    pass
# Create a black image, a window
# img = np.zeros((300,512,3), np.uint8)
image = cv2.imread("samples/match/7.png")

cv2.namedWindow('image')
cv2.createTrackbar('Blur','image',4,15,nothing)
cv2.createTrackbar('ret_1','image',1,500,nothing)
cv2.createTrackbar('ret_2','image',1,500,nothing)
cv2.createTrackbar('canny_1','image',0,500,nothing)
cv2.createTrackbar('canny_2','image',290,550,nothing)
cv2.createTrackbar("ON/OFF", 'image',1,1,nothing)

bg = image[0:345, 0:image.shape[0]]

x = 1.85
template = dyn_resize(image[345:384,0:150], 100 * x)
template = cv2.Canny(template,255,311)

times = template.shape[1] // round(36 * x)
templates = []
for i in range(times):
	temp = template[0:template.shape[1],round(x*i*36):round(x*(1 + i)*36)]
	if np.count_nonzero(temp) > 25:
		templates.append(temp)

cv2.imwrite('new.png', concatenate_image(templates))

while(1):
	img = bg.copy()
	

	k = cv2.waitKey(1) & 0xFF
	if k == 27: break

	# img = cv2.filter2D(img, -1, np.array([[-1,-1,-1], [-1,9.3,-1], [-1,-1,-1]]))


	blur = cv2.getTrackbarPos('Blur','image')
	# blur_image = cv2.blur(img, (blur, blur))
	if blur % 2 == 0:
		blur += 1
	# blur_image = cv2.medianBlur(img, blur)
	blur_image = cv2.GaussianBlur(img, (blur, blur), 0)


	ret_1 = cv2.getTrackbarPos('ret_1','image')
	ret_2 = cv2.getTrackbarPos('ret_2','image')
	# ret, thresh_img = cv2.threshold(img, ret_1, ret_2, cv2.THRESH_BINARY)

	canny_1 = cv2.getTrackbarPos('canny_1','image')
	canny_2 = cv2.getTrackbarPos('canny_2','image')

	img = cv2.Canny(blur_image,canny_1,canny_2)

	switcher = cv2.getTrackbarPos("ON/OFF",'image')

	if switcher == 0:
		img = bg.copy()
	else:
		i = 0
		for temp in templates:
			result = cv2.matchTemplate(img, temp, cv2.TM_CCOEFF_NORMED)
			(minVal, maxVal, minLoc, maxLoc) = cv2.minMaxLoc(result)

			rectangle(img, temp, minLoc, maxLoc)
			cv2.putText(img, str(i), maxLoc, cv2.FONT_HERSHEY_SIMPLEX, 1, (255,255,255), 2)
			i += 1

	# cv2.imshow('image', concatenate_image([closed, bg.copy]))
	cv2.imshow('image', img)


        
cv2.destroyAllWindows()
