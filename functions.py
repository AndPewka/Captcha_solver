import cv2
import numpy as np
import os
from PIL import Image
from io import BytesIO
import requests
import pytesseract
import copy

def dyn_resize(img, scale):
	width = int(img.shape[1] * scale / 100)
	height = int(img.shape[0] * scale / 100)
	dim = (width, height)
	return cv2.resize(img, dim, interpolation = cv2.INTER_AREA)

def fix_resize(img, width, height):
	dim = (width, height)
	return cv2.resize(img, dim, interpolation = cv2.INTER_AREA)

def getTextFromIMG(image):
	text = pytesseract.image_to_string(image)
	return text

def get_element(image, edge, width = 100, height = 100, resize = True):
	border = get_border(edge)
	image = image[border[0][1]:border[1][1],border[0][0]:border[1][0]]
	if resize: image = fix_resize(image, width, height)
	return image

def rectangle(image, template, minLoc, maxLoc):
  cv2.rectangle(image, (maxLoc[0], maxLoc[1]), (maxLoc[0] + template.shape[1], maxLoc[1] + template.shape[0]), (255, 0, 0), 1)


def get_border(edge):
	coords = get_coord(edge)
	min_x = 100000; max_x = 0; min_y = 100000; max_y = 0
	for coord in coords:
		x = coord[0]
		y = coord[1]

		if x < min_x: min_x = x
		if x > max_x: max_x = x
		if y < min_y: min_y = y
		if y > max_y: max_y = y
	return ((min_x, min_y),(max_x, max_y))

def concatenate_image(images, axis = 1):
	return np.concatenate(images, axis = axis)

def get_coord(edge):
	coords = []
	for poligon in edge:
		coords.append([poligon[0][0],poligon[0][1]])
	
	return coords

def show_image(image):
	cv2.imshow("Image", image)
	if cv2.waitKey(0) & 0xff == 27:
		cv2.destroyAllWindows()