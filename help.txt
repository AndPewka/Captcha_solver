# DOCKs                                                                                 #
# https://habr.com/ru/post/688316/                                                      #
# http://www.robindavid.fr/opencv-tutorial/cracking-basic-captchas-with-opencv.html     #
# https://docs.opencv.org/4.x/d7/d4d/tutorial_py_thresholding.html                      #
#########################################################################################

# открыть изображение
image = cv2.imread("samples/1.png")

# Изменить цветокор
image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

# Блюр
image = cv2.medianBlur(image, 3)
image = cv2.blur(image, (7, 7))
image = cv2.GaussianBlur(image, (7, 7), 0)

# Резкость (при 9.3 можно убрать тень)
image = cv2.filter2D(image, -1, np.array([[-1,-1,-1], [-1,9,-1], [-1,-1,-1]]))

# Фильтрация по каналу BGR
channel = image[:,:,0] 0 - G; 1 - G; 3 - R  
bin_img = np.zeros(image.shape)
bin_img[(channel > 200) * (channel <240)] = [0, 255, 0]


# в чб
ret, thresh_img = cv2.threshold(image_grey, color, bright, cv2.THRESH_BINARY)
color 0 - 255
bright 0 - 255
type = [cv2.THRESH_BINARY, cv2.THRESH_BINARY_INV, cv2.THRESH_TRUNC, cv2.THRESH_TOZERO, cv.THRESH_TOZERO_INV]

# поис контуров
contours, hierarchy = cv2.findContours(thresh_img, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

